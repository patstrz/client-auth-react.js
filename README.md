

Checkout this repo, install dependencies, then start the gulp process with the following:

```
	> clone
	> npm install
	> npm start
```