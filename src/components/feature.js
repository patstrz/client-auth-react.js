import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions';

class Feature extends Component {
  componentWillMount(){
    this.props.fetchHome();
  }
  render(){
    return(
      <div>
        <h1>This is a feature</h1>
        <p>{this.props.home}</p>
        </div>
    );
  }
}

function mapStateToProps(state){
  return { home: state.auth.home }
}

export default connect(mapStateToProps, actions)(Feature);
